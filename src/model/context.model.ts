export class DataEntitlementModel {
    firmCode: string;
    advisors: Array<string>;
}

export class ContextState {
    reportType: string;
    advisorCode: string;
}