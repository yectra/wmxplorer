import { BaseModel, DataRequest } from './base.model';

export class HoldingsModel extends BaseModel {
    description: string;
    ticker: string;
    cusip: string;
    weight: number;
    assestType: string;
    amount: string;
    status: string;
}

export class HoldingsRequestModel extends DataRequest {

}