import { BaseModel, DataRequest } from './base.model';

export class FeesModel extends BaseModel {
    client: string;
    type: string;
    custodian: string; 
    fees: string; 
    assets: string; 
    exclusions: string;
}

export class FeesInfoModel {
    totalFees: string;
    totalAssets: string;
    excludedFromBilling: string;
    advisoryRate: number;
    noOfAccounts: number;
    newRevenue: string;
    oldRevenue: string;
    totalBookAssets: number;
}

export class FeesRequestModel extends DataRequest {
}