import { BaseModel, DataRequest } from './base.model';

export class NotificationModel extends BaseModel {
    id: number;
    createdBy: string;
    createdTime: string;
    updatedBy: string;
    updatedTime: string;
    message: string;
    notifiedOn: Date;
    userName: string;
    totalAccounts: string;
    totalFees: number;
    totalAssets: number;
    action: string;
}

export class NotificationRequestModel extends DataRequest {
}