import { DataRequest } from './base.model';

export class UserModel {
    emailVerified: boolean;
    userName: string;
    fullName: string;
    preferredUsername: string;
    firstName: string;
    lastName: string;
    email: string;
}

export class UserRequestModel extends DataRequest {

}