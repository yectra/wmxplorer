import { BaseModel, DataRequest } from './base.model';

export class ClientModel extends BaseModel {
	id: number;
	client: string;
	price: string;
	advisorCode: string;
	custodian: string;
	since: string;
	noOfAccounts: number;
	assests: string;
}

export class ClientRequestModel extends DataRequest {
	
}