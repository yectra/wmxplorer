import { BaseModel } from './base.model';

export class SummaryModel extends BaseModel {
    totalClients: number;
    totalRevenue: number;
    change: number;
    reports: Array<ReportModel>;
    topClients: Array<TopClientModel>;
}

export class ReportModel {
    endDate: string;
    lostFee: number;
    newFee: number;
    onCycleFee: number;
    reportName: string;
    reportType: string;
    startDate: string;
    totalFee: number;
}

export class TopClientModel {
    householdName: string;
    totalValue: number;
}

export class ReportsRequestModel {
    reportType: string;
}