export class ChartModel {
    categories: Array<string> = [];

    fields: Array<ChartFieldModel> = [];
}

export class ChartFieldModel {
    name: string;
    data: Array<number> = [];

    color: string;
}