import { BaseModel, DataRequest } from './base.model';

export class FlowsModel extends BaseModel {
    account: string;
    type: string;
    cusip: string;
    date: string;
    assestType: string;
    amount: string;
    status: string;
}

export class FlowsRequestModel extends DataRequest {
    
}