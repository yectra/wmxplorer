import { BaseService, ServiceHelper } from './base.service'
import { IBaseModel, UserModel, UserRequestModel,  DataEntitlementModel } from '@/model';

export interface IUserService extends IBaseModel {
    getDataEntitlements(userName: string): Promise<Array<DataEntitlementModel>>;
}
export class UserService extends BaseService<UserRequestModel, UserModel> implements IUserService { 
    constructor() {
        super("user");
    }

    getDataEntitlements(userName: string) : Promise<Array<DataEntitlementModel>> {
        return this.httpGet('dataEntitlement', { userName: userName }, null).then(response => {
            return response.data;
         });
    }
}