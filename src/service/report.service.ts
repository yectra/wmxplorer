import { ServiceHelper } from './base.service'
import { IBaseModel,  SummaryModel, ReportsRequestModel } from '@/model';

export interface IReportService extends IBaseModel {
    getReport(request: ReportsRequestModel): Promise<SummaryModel>;
}
export class ReportService extends ServiceHelper implements IReportService { 
    getReport(request: ReportsRequestModel) : Promise<SummaryModel> {
        return this.httpGet('report', request, null).then(response => {
            return response.data;
         });
    }
}