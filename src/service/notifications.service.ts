import { IBaseService, BaseService, BaseMockService } from './base.service';
import { DataResponse, NotificationModel, NotificationRequestModel } from '@/model';

export interface INotificationsService extends IBaseService<NotificationRequestModel, NotificationModel> {
    getItems(request: NotificationRequestModel): Promise<DataResponse<NotificationModel>>;
}

export class NotificationsService extends BaseService<NotificationRequestModel, NotificationModel> implements INotificationsService {
    constructor() {
        super('notification')
    }

    // Needs to be removed once api is fixed to return in proper format
    getItems(request: NotificationRequestModel): Promise<DataResponse<NotificationModel>>{
        return this.httpGet(`${this.path}`, request).then(
            (response) => {
                let result = new DataResponse<NotificationModel>();
                result.data = response.data

                return result;
            }
        );
    }
}
