import { IBaseService, BaseService } from './base.service';
import { DataResponse, FlowsModel, FlowsRequestModel } from '@/model';

export interface IFlowsService extends IBaseService<FlowsRequestModel, FlowsModel> {

}

export class FlowsService extends BaseService<FlowsRequestModel, FlowsModel> {
}


export class FlowsMockService extends BaseService<FlowsRequestModel, FlowsModel> {

    public getItems(): Promise<DataResponse<FlowsModel>> {
        return new Promise((resolve, reject) => {
            let response = new DataResponse<FlowsModel>();
            response.data = [];
            response.data.push({account: "Jackie Kennedy's IRA (3978)", type: 'Deposit', cusip: '037833100', date: "6/13/2020", assestType: 'Equities', amount: '876,314,56', status: 'Experienced increase due to account deposit'});
            response.data.push({account: "Jackie Kennedy's Trust (5486)", type: 'Investing & Other activites', cusip: '037833100', date: "4/23/2020", assestType: 'Various', amount: '789,343.18', status: 'This holding is designated not billable '});
            response.data.push({account: "Jackie Kennedy's Joint (3978)", type: 'Withdrawal', cusip: '746916105', date: "8/24/2020", assestType: 'Fixed Income', amount: '467,932.17', status: 'Experienced decrease due to account termiation'});
            response.data.push({account: "Jackie Kennedy's Individual (3978)", type: 'Deposit', cusip: '399011731', date: "7/11/2020", assestType: 'Equities', amount: '99,423.88', status: 'Experienced decrease due to account termiation'});

            return resolve(response);
        });
    }

}