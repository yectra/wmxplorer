import { IBaseService, BaseService } from './base.service';
import { DataResponse, ClientRequestModel, ClientModel } from '@/model';

export interface IClientsService extends IBaseService<ClientRequestModel, ClientModel> {
}

export class ClientsService extends BaseService<ClientRequestModel, ClientModel> implements IClientsService {
    constructor() {
        super('client');
    }
}