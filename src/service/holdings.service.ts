import { IBaseService, BaseService } from './base.service';
import { DataResponse, HoldingsModel, HoldingsRequestModel } from '@/model';

export interface IHoldingsService extends IBaseService<HoldingsRequestModel, HoldingsModel> {

}

export class HoldingsService extends BaseService<HoldingsRequestModel, HoldingsModel> {
    
}
export class HoldingsMockService extends BaseService<HoldingsRequestModel, HoldingsModel> {

    public getItems(): Promise<DataResponse<HoldingsModel>> {
        return new Promise((resolve, reject) => {
            let response = new DataResponse<HoldingsModel>();
            response.data = [];
            response.data.push({description: 'Apple Inc', ticker: 'AAPL', cusip: '137833100', weight: 17.76, assestType: 'Equities', amount: '14,091,157.14', status: 'Experienced increase due to account deposit'});
            response.data.push({description: 'Goldman Sachs Alternative Premia Fund', ticker: 'GDIPX', cusip: '38145L885', weight: 4.12, assestType: 'Mutual Funds & ETFs', amount: '3,268,894.70', status: 'This holding is designated not billable '});
            response.data.push({description: 'Goldman Sachs Alternative Premia Fund', ticker: 'PNOPX', cusip: '746916105', weight: 10.52, assestType: 'Fixed Income', amount: '8,346,789.38', status: 'Experienced decrease due to account termiation'});
            response.data.push({description: 'Google - Alphabet Inc. Classic A Common Stock', ticker: 'GOOGL', cusip: '38259P508', weight: 69.25, assestType: 'Equities', amount: '55,139,543.31', status: 'Experienced decrease due to account termiation'});

            return resolve(response);
        });
    }

}