import { IBaseService, BaseService } from './base.service';
import { DataResponse, FeesModel, FeesRequestModel } from '@/model';

export interface IFeesService extends IBaseService<FeesRequestModel, FeesModel> {

}

export class FeesService extends BaseService<FeesRequestModel, FeesModel> implements IFeesService {

}

export class FeesMockService extends BaseService<FeesRequestModel, FeesModel> implements IFeesService {

    public getItems(): Promise<DataResponse<FeesModel>> {
        return new Promise((resolve, reject) => {
            let response = new DataResponse<FeesModel>();
            response.data = [];
            response.data.push({client: "Jackie Kennedy's IRA (3978)", type: 'IRA', custodian: 'Fidelity', fees: '24,811.87', assets: '21,983,331.76', exclusions: '287,944.32'});
            response.data.push({client: "Jackie Kennedy's Trust (5486)", type: 'Trust', custodian: 'JP Morgan', fees: '16,673.93', assets: '18,133,578.12', exclusions: '36,521.11'});
            response.data.push({client: "Jackie Kennedy's Joint (3978)", type: 'Joint Account', custodian: 'JP Morgan', fees: '16,673.93', assets: '18,133,578.12', exclusions: '36,521.11'});
            response.data.push({client: "Jackie Kennedy's Individual (3978)", type: 'Joint Account', custodian: 'Morgan Stanley', fees: '16,673.93', assets: '18,133,578.12', exclusions: '36,521.11'});

            return resolve(response);
        });
    }
}