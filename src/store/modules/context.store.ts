import { GetterTree, MutationTree, ActionTree } from 'vuex';
import { ContextState } from '@/model';

const state: ContextState = {
    reportType: '',
    advisorCode: localStorage.getItem('advisorCode') || 'All',
}

const getters: GetterTree<ContextState, any> = {
    reportType: state => {
        return state.reportType
    },
    advisorCode: state => {
        return state.advisorCode
    }
}

const mutations: MutationTree<ContextState> = {
    onReportTypeChanged: (state, reportType) => {
        state.reportType = reportType;
    },

    onAdvisorCodeChanged: (state, advisorCode) => {
        localStorage.setItem('advisorCode', advisorCode);

        state.advisorCode = advisorCode;
    }
}

const actions: ActionTree<ContextState, any> = {
    reportTypeChanged(context, reportType) {
        context.commit('onReportTypeChanged', reportType)
    },

    advisorCodeChanged(context, advisorCode) {
        context.commit('onAdvisorCodeChanged', advisorCode)
    }
}

export const ContextModule = {
    state,
    getters,
    mutations,
    actions
}