import { Vue } from "vue-property-decorator";
import { AxiosResponse, AxiosError } from "axios";
import { AlertType, UserModel } from '@/model';

export class BaseComponent extends Vue {
    error(e: AxiosError) {
        let root: any = this.$root;

        root.$alert(e.message, AlertType.Error);

        this.hideLoading();
    }

    showError(message: string) {
        let root: any = this.$root;

        root.$alert(message, AlertType.Error);
    }

    alert(message: string, type: AlertType) {
        let root: any = this.$root;

        root.$alert(message, type);
    }

    showLoading(message?: string) {
        let root: any = this.$root;

        root.$showLoading(message);
    }

    hideLoading() {
        let root: any = this.$root;

        root.$hideLoading();
    }

    confirm(title: string, message: string) {
        let root: any = this.$root;

        return root.$confirm(title, message);
    }

}