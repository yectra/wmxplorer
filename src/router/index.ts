import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router';
import store from '@/store';
import axios from "axios";

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: () => import('../views/home/Index.vue'),
  },
  {
    path: '/notifications',
    name: 'Notifications',
    component: () => import('../views/notification/Index.vue'),
  },
  {
    path: '/clients',
    name: 'Clients',
    component: () => import('../views/clients/clients-list.vue')
  },
  {
    path: '/clients/:id',
    component: () => import('../views/clients/clients-details.vue'),
    redirect: '/clients/:id/fees',
    children: [
      {
        path: 'fees',
        name: 'Fees',
        component: () => import('../views/clients/components/fees.vue')
      },
      {
        path: 'holdings',
        name: 'Holdings',
        component: () => import('../views/clients/components/holdings.vue')
      },
      {
        path: 'flows',
        name: 'Flows',
        component: () => import('../views/clients/components/flows.vue')
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => !record.meta.anonymous)) {
    axios.defaults.headers.common["advisors"] = store.getters.advisorCode;

    if (store.getters.isLoggedIn) {
      let token = store.getters.accessToken;
      if (token)
        axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
      next()
      return
    }

    store.dispatch("login").then(() => {
      if (store.getters.isLoggedIn) {
        let token = store.getters.accessToken;
        if (token)
          axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
        next("/")
      }
      else
        next("/login")
    });

  } else {
    next()
  }
});