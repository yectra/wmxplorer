import { Vue, Provide } from "vue-property-decorator";

import { IUserService, UserService } from '@/service';
import { IReportService, ReportService } from '@/service';
import { INotificationsService, NotificationsService } from '@/service';
import { IClientsService, ClientsService } from '@/service';
import { IFeesService, FeesService, FeesMockService } from '@/service';
import { IHoldingsService, HoldingsService, HoldingsMockService } from '@/service';
import { IFlowsService, FlowsService, FlowsMockService } from '@/service';
``
export class DIContainer extends Vue {
    @Provide('userService') repCodeService: IUserService = new UserService();
    @Provide('reportService') reportService: IReportService = new ReportService();
    @Provide('notificationsService') notificationsService: INotificationsService = new NotificationsService();
    @Provide('clientsService') clientsService: IClientsService = new ClientsService();
    @Provide('feesService') feesService: IFeesService = new FeesMockService('');
    @Provide('holdingsService') holdingsService: IHoldingsService = new HoldingsMockService('');
    @Provide('flowsService') flowsService: IFlowsService = new FlowsMockService('');
}